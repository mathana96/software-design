
public class Account
{
  int number;
  int balance;
  
  public Account(int number, int balance)
  {
    this.number = number;
    this.balance = balance;
  }
  
  public int getBalance()
  {
    return this.balance;
  }
  
  public int updateBalance(int newBalance)
  {
    this.balance = newBalance;
    return this.balance;
  }
  

}
