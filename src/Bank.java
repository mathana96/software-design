import java.util.HashMap;
import java.util.Map;

public class Bank
{
  public Map<Integer, Customer> records = new HashMap<>();
  
  public Bank()
  {
    
  }
  
  public Map<Integer, Customer> getRecords()
  {
    return this.records;
  }
}
