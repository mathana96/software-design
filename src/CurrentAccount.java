
public class CurrentAccount extends Account
{
  int passcode;
  
  public CurrentAccount(int number, int balance, int passcode)
  {
    super(number, balance);
    this.passcode = passcode;
  }
  
  public int getPasscode()
  {
    return this.passcode;
  }
  
}
