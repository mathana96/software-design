public class Customer
{
  CurrentAccount currentAccount;
  DepositAccount depositAccount;
  
  String name;
  String address;
  String dob;
  Integer id;

  public Customer(String name, String address, String dob, Integer id)
  {
    this.name = name;
    this.address = address;
    this.dob = dob;
    this.id = id;
  }
  
  public void createCurrentAccount(int number, int balance, int passcode)
  {
    currentAccount = new CurrentAccount(number, balance, passcode);
  }
  
  public void createDepositAccount(int number, int balance)
  {
    depositAccount = new DepositAccount(number, balance);
  }
  
  public CurrentAccount getCurrentAccount()
  {
    return currentAccount;
  }
  
  public DepositAccount getDepositAccount()
  {
    return depositAccount;
  }
  
}
