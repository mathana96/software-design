import java.util.Map;

public class Transaction
{
  public Transaction()
  {
    
  }
  
  public boolean validate(Integer id, int passcode, Bank bank)
  {
    Map<Integer, Customer> records = bank.getRecords();
    
    Customer theCustomer = records.get(id);
 
    int realPasscode = theCustomer.currentAccount.getPasscode();
    
    if (passcode == realPasscode)
      return true;
    else
      return false;
   
   
  }
  
  public int checkBalance(Integer id, int passcode, Bank bank)
  {
    Map<Integer, Customer> records = bank.getRecords();
    
    Customer theCustomer = records.get(id);
 
    int balance = theCustomer.currentAccount.getBalance();
    
    return balance;
  }
  
  public int withdrawMoney(Integer id, int passcode, Bank bank, int amount)
  {
    
    Map<Integer, Customer> records = bank.getRecords();
    
    Customer theCustomer = records.get(id);
    CurrentAccount currentAccount = theCustomer.currentAccount;
    
    int balance = checkBalance(id, passcode, bank);
    
    boolean balanceVerified = verifyBalance(balance, amount);
    boolean notesVerified = verifyNotes();
    
    
    if (balanceVerified && notesVerified)
    {
      int newBalance = balance - amount;

      return currentAccount.updateBalance(newBalance);
    }
    else
    {
      return -1;
    }

  }
  
  public boolean verifyBalance(int balance, int amount)
  {
    if (balance >= amount)
      return true;
    else
      return false;
 
  }
  
  /**
   * Always returns True as it is assumed that the ATM always has money, for the sake of testing, POC
   * @return true
   */
  public boolean verifyNotes()
  {
    return true;
  }
  
  public int lodgeMoney(Integer id, int passcode, Bank bank, int lodgement)
  {
    Map<Integer, Customer> records = bank.getRecords();
    
    Customer theCustomer = records.get(id);
    CurrentAccount currentAccount = theCustomer.currentAccount;
    
    int oldBalance = currentAccount.getBalance();
    int newBalance = oldBalance + lodgement;
    
    return currentAccount.updateBalance(newBalance);
  }
  
  public int transferMoney(Integer id, int passcode, Bank bank, int amount, int option)
  {
    Map<Integer, Customer> records = bank.getRecords();
    
    Customer theCustomer = records.get(id);
    
    if (option == 1) //From current to deposit
    {
      CurrentAccount currentAccount = theCustomer.currentAccount;
      DepositAccount depositAccount = theCustomer.depositAccount;
      
      if (verifyBalance(currentAccount.balance, amount))
      {
        currentAccount.updateBalance(currentAccount.balance-amount);
        depositAccount.updateBalance(depositAccount.balance+amount);
      }  
      
      return depositAccount.getBalance();
    }
    else if (option == 2)
    {
      CurrentAccount currentAccount = theCustomer.currentAccount;
      DepositAccount depositAccount = theCustomer.depositAccount;
      
      if (verifyBalance(depositAccount.balance, amount))
      {
        currentAccount.updateBalance(currentAccount.balance+amount);
        depositAccount.updateBalance(depositAccount.balance-amount);
      } 
      
      return currentAccount.getBalance();
    }
   
    return -1;
    
  }
}
