import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import javafx.scene.chart.PieChart.Data;

public class ATMTest
{

  ATM atm;
  SetupData data;
  Bank bank;
  
  
  @Before
  public void setup()
  {
    data = new SetupData();
  }
  
  @Test
  public void authenticateTest()
  {
    bank = data.getData();
    atm = new ATM();
    
    assertEquals("I iz optionz", atm.authenticate(1, 1234, bank));
  }

  @Test
  public void displayBalanceTest()
  {
    bank = data.getData();
    atm = new ATM();
    
    assertEquals(10000000, atm.displayBalance(1, 1234, bank));
  }
  
  @Test
  public void displayOptionsTest()
  {
    atm = new ATM();
    
    assertEquals("I iz optionz", atm.displayOptions());
    
  }
  
  @Test
  public void displayWithdrawOptionsTest()
  {
    atm = new ATM();
    
    assertEquals("I iz displayWithdrawOptionz", atm.displayWithdrawOption());
    
  }
}
