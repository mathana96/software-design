import static org.junit.Assert.*;

import org.junit.Test;

public class CurrentAccountTest
{
  CurrentAccount currentAccount;
  @Test
  public void testGetPasscode()
  {
    currentAccount = new CurrentAccount(1111, 1000000000, 1234);
    
    assertEquals(1234, currentAccount.getPasscode());
  }

  @Test
  public void testGetBalance()
  {
    currentAccount = new CurrentAccount(1111, 1000000000, 1234);
    
    assertEquals(1000000000, currentAccount.getBalance());
  }
  
  @Test
  public void testUpdateBalance()
  {
    currentAccount = new CurrentAccount(1111, 1000000000, 1234);
    int newBalance = 100;
    assertEquals(100, currentAccount.updateBalance(newBalance));
  }
}
