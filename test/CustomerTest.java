import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest
{

  @Test
  public void createCustomertest()
  {
    Customer customer = new Customer("Cust1", "addr1", "dob1", 1);
    
    customer.createCurrentAccount(1000, 10000000, 1234);
    
    customer.createDepositAccount(2000, 10000);
    
    assertEquals("Cust1", customer.name);
    assertEquals("addr1", customer.address);
    assertEquals("dob1", customer.dob);
    
    assertEquals(10000000, customer.currentAccount.getBalance());
    assertEquals(10000, customer.depositAccount.getBalance());
  }

}
