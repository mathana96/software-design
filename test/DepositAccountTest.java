import static org.junit.Assert.*;

import org.junit.Test;

public class DepositAccountTest
{

  @Test
  public void getBalancetest()
  {
    DepositAccount depositAccount = new DepositAccount(1234, 1212);
    assertEquals(1212, depositAccount.getBalance());
  }

}
