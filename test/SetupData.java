
public class SetupData
{
  Customer customer1;
  Customer customer2;
  Bank bank;
  Transaction transaction;
  
  public SetupData()
  {
    bank = new Bank();
    customer1 = new Customer("Cust1", "addr1", "dob1", 1);
    customer2 = new Customer("Cust2", "addr2", "dob2", 2);
    
    customer1.createCurrentAccount(1000, 10000000, 1234);
    customer2.createCurrentAccount(1001, 20000000, 4567);
    
    customer1.createDepositAccount(2000, 10000);
    
    bank.records.put(customer1.id, customer1);
    bank.records.put(customer2.id, customer2); 
  }
  
  public Bank getData()
  {
    return bank;
  }
}
