import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TransactionTest
{

  Bank bank;
  Transaction transaction;
  SetupData data;
  
  @Before
  public void setup()
  {
    data = new SetupData();
  }
   
  
  @Test
  public void validateTest()
  {
    bank = data.getData();
 
    int testId1 = 1;
    int testPasscode1 = 1234;
   
    transaction = new Transaction();
    assertTrue(transaction.validate(testId1, testPasscode1, bank));
 
    
    
  }
  
  @Test
  public void checkBalanceTest()
  {
    bank = data.getData();
 
    int testId1 = 1;
    int testPasscode1 = 1234;
   
    transaction = new Transaction();
    assertEquals(10000000, transaction.checkBalance(testId1, testPasscode1, bank));   
    
  }
  
  @Test
  public void withdrawMoneyTest()
  {
    bank = data.getData();
 
    int testId1 = 1;
    int testPasscode1 = 1234;
    
    int amount = 10000000;
    int balance = amount - 100000;
   
    transaction = new Transaction();
   
    assertEquals(balance, transaction.withdrawMoney(testId1, testPasscode1, bank, 100000));   
    
  }
  
  @Test
  public void lodgeMoneyTest()
  {
    bank = data.getData();
 
    int testId1 = 1;
    int testPasscode1 = 1234;
    
    int amount = 10000000;
    int lodgement = 100000;
   
    transaction = new Transaction();
   
    assertEquals(amount+lodgement, transaction.lodgeMoney(testId1, testPasscode1, bank, 100000));   
    
  }
  
  @Test
  public void transferMoneyFromCurrentToDepositTest()
  {
    bank = data.getData();
 
    int testId1 = 1;
    int testPasscode1 = 1234;
    int amount = 1000;
    int finalAmount = 10000 + amount;
   
    transaction = new Transaction();
   
    int option = 1; //From current to deposit
    
    assertEquals(finalAmount, transaction.transferMoney(testId1, testPasscode1, bank, amount, option));   
    
  }

  @Test
  public void transferMoneyFromDepositToCurrentTest()
  {
    bank = data.getData();
 
    int testId1 = 1;
    int testPasscode1 = 1234;
    int amount = 1000;
    int finalAmount = 10000000 + amount;
   
    transaction = new Transaction();
   
    int option = 2; //From current to deposit
    
    assertEquals(finalAmount, transaction.transferMoney(testId1, testPasscode1, bank, amount, option));   
    
  }
  
}
